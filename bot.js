const TOKEN = "MTIxNzE0NDUwNzg0MjQyODkyOQ.GhCWOU.5ewpZcIDbtDHuiFYvySkkuFs213w6SpS5_whHk";
const CLIENT_ID = "1217144507842428929";
const URL = "https://discordapp.com/oauth2/authorize?&client_id=1217144507842428929&scope=bot&permissions=395405428800";

const { REST, Routes, ActionRowBuilder, ButtonBuilder, ButtonStyle, PermissionsBitField, EmbedBuilder } = require('discord.js');
var fs = require('fs');

const { Client, GatewayIntentBits } = require('discord.js');
const { isAsyncFunction } = require('util/types');
const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMembers] });

var data = {};

var admin = "124136556578603009";
const status = { none: 0, lobby: 1, playing: 3 };

let piecesPerTeam =
    [
        { name: "Vlag", amount: 1, rank: 0 },
        { name: "Maarschalk", amount: 1, rank: 1 }
    ];

let additionalPieces =
    [
        { name: "Spion", amount: 1, rank: 11 },
        { name: "Bom", amount: 6, rank: 10 },
        { name: "Mineur", amount: 5, rank: 9 },
        { name: "Generaal", amount: 1, rank: 2 },
        { name: "Kolonel", amount: 2, rank: 3 },
        { name: "Majoor", amount: 3, rank: 4 },
        { name: "Verkenner", amount: 8, rank: 8 },
        { name: "Kapitein", amount: 4, rank: 5 },
        { name: "Luitenant", amount: 4, rank: 6 },
        { name: "Sergeant", amount: 4, rank: 7 }
    ];

shuffleArray(additionalPieces);

piecesPerTeam.push(...additionalPieces);

var maxPieces = 40;

const commands = [
    {
        name: 'ping',
        description: 'Zegt pong!',
        execute: function (interaction)
        {
            interaction.reply('Pong!');
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: "autoinschrijfuitnodiging",
        description: "Nodig mensen uit om altijd mee te doen!",
        execute: async function (interaction)
        {
            const joinButton = new ButtonBuilder()
                .setCustomId("autojoin")
                .setLabel("Schrijf je in!")
                .setStyle(ButtonStyle.Primary);

            const row = new ActionRowBuilder()
                .addComponents(joinButton);

            interaction.reply({
                content: "# Doe mee met Stratego!\nKlik op de onderstaande knop om je in te schrijven op toekomstige rondes **Levend Stratego: Tweedracht Editie**! Je wordt dan automatisch ingeschreven wanneer er een spel start.\n-# Dit schrijft je __niet__ in voor een lopende inschrijfperiode! Dat moet je nog zelf doen!",
                components: [row],
            });
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: "startspel",
        description: "Begin de inschrijfperiode voor een nieuw spel",
        execute: async function (interaction)
        {
            data.servers = data.servers || {};
            data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
            data.servers[interaction.guildId].gameStatus = data.servers[interaction.guildId].gameStatus || status.none;
            data.servers[interaction.guildId].autojoin = data.servers[interaction.guildId].autojoin || [];

            if (data.servers[interaction.guildId].gameStatus >= status.lobby)
            {
                interaction.reply({
                    content: `Er is al een spel gestart!`, ephemeral: true
                });
                return;
            }

            await RemoveAllRoles(interaction.guildId, interaction.guild);

            data.servers[interaction.guildId].gameStatus = status.lobby;

            const joinButton = new ButtonBuilder()
                .setCustomId("join")
                .setLabel("Doe mee")
                .setStyle(ButtonStyle.Primary);

            const startButton = new ButtonBuilder()
                .setCustomId("startnow")
                .setLabel("Begin spel")
                .setStyle(ButtonStyle.Secondary);

            const row = new ActionRowBuilder()
                .addComponents(joinButton, startButton);

            shuffleArray(data.servers[interaction.guildId].autojoin);

            for (let i = data.servers[interaction.guildId].autojoin.length - 1; i >= 0; i--)
            {
                const isMember = await interaction.guild.members.fetch(data.servers[interaction.guildId].autojoin[i]).then(() => true).catch(() => false);
                if (!isMember)
                {
                    data.servers[interaction.guildId].autojoin.splice(i, 1);
                    continue;
                }

                let sortToBlue = data.servers[interaction.guildId].teams.red.length > data.servers[interaction.guildId].teams.blue.length;

                if (sortToBlue)
                {
                    data.servers[interaction.guildId].teams.blue.push({ userId: data.servers[interaction.guildId].autojoin[i] });
                }
                else
                {
                    data.servers[interaction.guildId].teams.red.push({ userId: data.servers[interaction.guildId].autojoin[i] });
                }
            }

            interaction.reply({
                content: `# Tijd voor Levend Stratego: Tweedracht Editie!\n## Schrijf je in met de knop op dit bericht. Je krijgt bericht zodra het spel begint!\nAantal automatisch ingeschreven spelers: ` + data.servers[interaction.guildId].autojoin.length,
                components: [row],
            });
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: "iedereenmeedoen",
        description: "Ze hebben geen keuze",
        execute: async function (interaction)
        {
            let membersToAddColl = await interaction.channel.members.fetch();
            let membersToAdd = [...membersToAddColl]
            let totalPlayers = 0;

            interaction.deferReply();

            for (let m = 0, len = membersToAdd.length; m < len; m++)
            {
                let userId = membersToAdd[m][0];
                let canAdd = true;

                let maybeUser = await GetUserById(interaction.guildId, userId);
                if (maybeUser.bot)
                {
                    continue;
                }

                for (let i = 0; i < data.servers[interaction.guildId].teams.red.length; i++)
                {
                    if (data.servers[interaction.guildId].teams.red[i].userId == userId)
                    {
                        canAdd = false;
                        break;
                    }
                }

                for (let i = 0; i < data.servers[interaction.guildId].teams.blue.length; i++)
                {
                    if (data.servers[interaction.guildId].teams.blue[i].userId == userId)
                    {
                        canAdd = false;
                        break;
                    }
                }

                if (data.servers[interaction.guildId].gameStatus != status.lobby)
                {
                    interaction.editReply({ content: "Ja dat kan nu dus niet.", ephemeral: true });
                    return;
                }

                if (!canAdd)
                    continue;

                let sortToBlue = data.servers[interaction.guildId].teams.red.length > data.servers[interaction.guildId].teams.blue.length;

                if (sortToBlue)
                {
                    data.servers[interaction.guildId].teams.blue.push({ userId: userId });
                }
                else
                {
                    data.servers[interaction.guildId].teams.red.push({ userId: userId });
                }

                totalPlayers = data.servers[interaction.guildId].teams.red.length + data.servers[interaction.guildId].teams.blue.length;

                if (totalPlayers == 80)
                {
                    let failed = StartGame(interaction.guildId, interaction.guild);

                    let embedRedPublic = new EmbedBuilder()
                        .setColor(0xFF0000)
                        .setTitle('Tiem Rood');
                    let embedBluePublic = new EmbedBuilder()
                        .setColor(0x0000FF)
                        .setTitle('Tiem Blauw');

                    redDescPublic = [];
                    blueDescPublic = [];

                    for (let i = 0; i < data.servers[interaction.guildId].teams.red.length; i++)
                    {
                        let user = await GetUserById(interaction.guild, data.servers[interaction.guildId].teams.red[i].userId);
                        let userName = user.displayName;

                        redDescPublic.push(userName);
                    }

                    for (let i = 0; i < data.servers[interaction.guildId].teams.blue.length; i++)
                    {
                        let user = await GetUserById(interaction.guild, data.servers[interaction.guildId].teams.blue[i].userId);
                        let userName = user.displayName;

                        blueDescPublic.push(userName);
                    }

                    shuffleArray(redDescPublic);
                    shuffleArray(blueDescPublic);

                    embedRedPublic.setDescription(redDescPublic.join("\n") || "leeg");
                    embedBluePublic.setDescription(blueDescPublic.join("\n") || "leeg");

                    interaction.editReply({ embeds: [embedRedPublic, embedBluePublic], content: "## De tiems zijn vol!\n# De strijd begint!\n\n" + (failed.length > 0 ? "Spelers die niet mee konden doen door een onbekende fout:\n" + failed.join("\n") : "") });
                    return;
                }
            }

            interaction.editReply({ content: "## Iedereen doet mee!\n\n" + "We hebben nu " + totalPlayers + " spelers!\n-# Gezellig hè?" });
        }
    },
    {
        name: "autoinschrijf",
        description: "Doe automagisch altijd mee!",
        execute: async function (interaction)
        {
            data.servers = data.servers || {};
            data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
            data.servers[interaction.guildId].autojoin = data.servers[interaction.guildId].autojoin || [];

            if (data.servers[interaction.guildId].autojoin.indexOf(interaction.user.id) < 0)
            {
                data.servers[interaction.guildId].autojoin.push(interaction.user.id);
                interaction.reply({ content: "Je wordt vanaf nu automatisch ingeschreven!", ephemeral: true });
            }
            else
            {
                interaction.reply({ content: "Je werd al automatisch ingeschreven!", ephemeral: true });
            }
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: "autouitschrijf",
        description: "Doe niet meer automagisch mee!",
        execute: async function (interaction)
        {
            data.servers = data.servers || {};
            data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
            data.servers[interaction.guildId].autojoin = data.servers[interaction.guildId].autojoin || [];

            let index = data.servers[interaction.guildId].autojoin.indexOf(interaction.user.id);

            if (index >= 0)
            {
                data.servers[interaction.guildId].autojoin.splice(index, 1);
                interaction.reply({ content: "Je wordt vanaf nu niet meer automatisch ingeschreven!", ephemeral: true });
            }
            else
            {
                interaction.reply({ content: "Je werd al niet automatisch ingeschreven!", ephemeral: true });
            }
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: "stopspel",
        description: "Stop het spel onmiddelijk.",
        admin: true,
        execute: async function (interaction)
        {
            data.servers = data.servers || {};
            data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
            data.servers[interaction.guildId].gameStatus = data.servers[interaction.guildId].gameStatus || status.none;

            if (data.servers[interaction.guildId].gameStatus == status.none)
            {
                interaction.reply({ content: "Er is geen actief spel!", ephemeral: true });
                return;
            }

            interaction.reply({ content: "## Het spel is stopgezet." });

            await StopGame(interaction.guildId, interaction.guild);
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: "aanvallen",
        description: "Val iemand aan!",
        options: [{ type: 6, name: "gebruiker", description: "Wiemst", required: true }],
        execute: async function (interaction)
        {
            data.servers = data.servers || {};
            data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
            data.servers[interaction.guildId].admins = data.servers[interaction.guildId].admins || [];
            data.servers[interaction.guildId].teams = data.servers[interaction.guildId].teams || { red: [], blue: [] };
            data.servers[interaction.guildId].gameStatus = data.servers[interaction.guildId].gameStatus || status.none;

            let blueRole = await GetRoleForTeam(interaction.guild, data.servers[interaction.guildId].blueRole);
            let redRole = await GetRoleForTeam(interaction.guild, data.servers[interaction.guildId].redRole);

            if (redRole && !interaction.channel.permissionsFor(redRole).has(PermissionsBitField.Flags.ViewChannel))
                return interaction.reply({ content: "Je mag niet aanvallen in kanalen die Tiem Rood niet kan zien.", ephemeral: true });

            if (blueRole && !interaction.channel.permissionsFor(blueRole).has(PermissionsBitField.Flags.ViewChannel))
                return interaction.reply({ content: "Je mag niet aanvallen in kanalen die Tiem Blauw niet kan zien.", ephemeral: true });

            let targetId = interaction.options.data[0].value;
            let attacker = null;
            let defender = null;
            let blueDefend = false;
            let blueAttack = false;

            for (let i = 0; i < data.servers[interaction.guildId].teams.red.length; i++)
            {
                if (data.servers[interaction.guildId].teams.red[i].userId == targetId)
                    defender = data.servers[interaction.guildId].teams.red[i];
                else if (data.servers[interaction.guildId].teams.red[i].userId == interaction.user.id)
                    attacker = data.servers[interaction.guildId].teams.red[i];
            }

            for (let i = 0; i < data.servers[interaction.guildId].teams.blue.length; i++)
            {
                if (data.servers[interaction.guildId].teams.blue[i].userId == targetId)
                {
                    defender = data.servers[interaction.guildId].teams.blue[i];
                    blueDefend = true;
                }
                else if (data.servers[interaction.guildId].teams.blue[i].userId == interaction.user.id)
                {
                    attacker = data.servers[interaction.guildId].teams.blue[i];
                    blueAttack = true;
                }
            }

            if (!attacker)
                return interaction.reply({ content: "Wdn je doet geneens mee, wat probeer je aan te vallen?", ephemeral: true });

            if (!defender)
                return interaction.reply({ content: "Je doelwit doet niet mee aan het spel. Waar ben je nou mee bezig?", ephemeral: true });

            if (blueAttack == blueDefend)
                return interaction.reply({ content: "Wat probeer je je eigen tiem aan te vallen. Ben je niet lekker?", ephemeral: true });

            if (attacker.piece.name == "Vlag" || attacker.piece.name == "Bom")
                return interaction.reply({ content: "Je kunt niet aanvallen! Probeer de ander uit te lokken.", ephemeral: true });

            let killText = "Je bent gedood door:\n### ";
            let killTitle = "";
            let killTitleDm = "";
            let defendTitle = "";

            IncreaseUserStat(interaction.guildId, interaction.user.id, "Hoe vaak aangevallen:", 1);

            for (let i = 0; i < data.servers[interaction.guildId].teams.red.length; i++)
            {
                if (data.servers[interaction.guildId].teams.red[i].userId == interaction.user.id)
                {
                    killTitleDm = attacker.piece.name + " " + interaction.user.displayName + " van Tiem Rood";
                    killTitle = attacker.piece.name + " <@" + attacker.userId + "> van Tiem Rood";
                }

                if (data.servers[interaction.guildId].teams.red[i].userId == defender.userId)
                {
                    defendTitle = defender.piece.name + " <@" + defender.userId + "> van Tiem Rood";
                }
            }

            for (let i = 0; i < data.servers[interaction.guildId].teams.blue.length; i++)
            {
                if (data.servers[interaction.guildId].teams.blue[i].userId == interaction.user.id)
                {
                    killTitleDm = attacker.piece.name + " " + interaction.user.displayName + " van Tiem Blauw";
                    killTitle = attacker.piece.name + " <@" + attacker.userId + "> van Tiem Blauw";
                }

                if (data.servers[interaction.guildId].teams.blue[i].userId == defender.userId)
                {
                    defendTitle = defender.piece.name + " <@" + defender.userId + "> van Tiem Blauw";
                }
            }

            killText += "<@" + attacker.userId + ">!";

            if (defender.piece.name == "Vlag")
            {
                IncreaseUserStat(interaction.guildId, interaction.user.id, "Vlaggen gekaapt:", 1);
                await interaction.reply({ content: "# DE VLAG VAN TIEM " + (blueDefend ? "BLAUW" : "ROOD") + " IS GEKAAPT!\n## Tiem " + (blueDefend ? "Rood" : "Blauw") + " heeft gewonnen!" });
                await StopGame(interaction.guildId, interaction.guild);
                return;
            }
            else if (defender.piece.name == "Bom")
            {
                if (attacker.piece.name == "Mineur")
                {
                    IncreaseUserStat(interaction.guildId, interaction.user.id, "Bommen ontmanteld:", 1);
                    await interaction.reply("# " + defendTitle + " is gedood door <@" + attacker.userId + ">!");
                    await KillPlayer(interaction.guildId, interaction.guild, defender.userId, killText);
                }
                else
                {
                    IncreaseUserStat(interaction.guildId, interaction.user.id, "Hoe vaak op een bom getrapt:", 1);
                    IncreaseUserStat(interaction.guildId, interaction.user.id, "Spelers gedood:", 1);
                    await interaction.reply("# " + killTitle + " is gedood tijdens een aanval op " + (attacker.piece.name == "Verkenner" ? defendTitle : "<@" + defender.userId + ">") + "!");
                    await KillPlayer(interaction.guildId, interaction.guild, attacker.userId, "Je bent ten onder gegaan in een gevecht met " + "<@" + defender.userId + ">");
                }
            }
            else if (defender.piece.name == "Maarschalk" && attacker.piece.name == "Spion")
            {
                IncreaseUserStat(interaction.guildId, interaction.user.id, "Spelers gedood:", 1);
                await interaction.reply("# " + defendTitle + " is gedood door <@" + attacker.userId + ">!");
                await KillPlayer(interaction.guildId, interaction.guild, defender.userId, killText);
            }
            else if (attacker.piece.rank < defender.piece.rank)
            {
                IncreaseUserStat(interaction.guildId, interaction.user.id, "Spelers gedood:", 1);
                await interaction.reply("# " + defendTitle + " is gedood door <@" + attacker.userId + ">!");
                await KillPlayer(interaction.guildId, interaction.guild, defender.userId, killText);
            }
            else if (attacker.piece.rank == defender.piece.rank)
            {
                IncreaseUserStat(interaction.guildId, interaction.user.id, "Spelers gedood:", 1);
                await interaction.reply("# " + defendTitle + " en " + killTitle + " hebben elkaar gedood in een gelijke strijd!");
                await KillPlayer(interaction.guildId, interaction.guild, defender.userId, "Je bent in een gelijke strijd ten onder gegaan in een gevecht met " + killTitle);
                await KillPlayer(interaction.guildId, interaction.guild, attacker.userId, "Je bent in een gelijke strijd ten onder gegaan in een gevecht met " + defendTitle);
            }
            else if (attacker.piece.rank > defender.piece.rank)
            {
                IncreaseUserStat(interaction.guildId, interaction.user.id, "Geprobeerd een te sterke speler te doden:", 1);
                await interaction.reply("# " + killTitle + " is gedood tijdens een aanval op " + (attacker.piece.name == "Verkenner" ? defendTitle : "<@" + defender.userId + ">") + "!");
                await KillPlayer(interaction.guildId, interaction.guild, attacker.userId, "Je bent ten onder gegaan in een gevecht met " + "<@" + defender.userId + ">");
            }

            for (let i = 0; i < data.servers[interaction.guildId].teams.red.length; i++)
            {
                if (data.servers[interaction.guildId].teams.red[i].piece.name != "Bom" && data.servers[interaction.guildId].teams.red[i].piece.name != "Vlag")
                    return;
            }

            for (let i = 0; i < data.servers[interaction.guildId].teams.blue.length; i++)
            {
                if (data.servers[interaction.guildId].teams.blue[i].piece.name != "Bom" && data.servers[interaction.guildId].teams.blue[i].piece.name != "Vlag")
                    return;
            }

            await interaction.followUp("Er zijn geen spelers meer in het spel die aan kunnen vallen!\n# Het is gelijkspel!");
            await StopGame(interaction.serverId, interaction.guild);
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: "wiemst",
        description: "Wat is je rol, en in welk tiem zit je?",
        execute: async function (interaction)
        {
            data.servers = data.servers || {};
            data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
            data.servers[interaction.guildId].gameStatus = data.servers[interaction.guildId].gameStatus || status.none;
            data.servers[interaction.guildId].teams = data.servers[interaction.guildId].teams || { red: [], blue: [] };

            if (data.servers[interaction.guildId].gameStatus == status.none)
            {
                interaction.reply({ content: "Er is geen actief spel!", ephemeral: true });
                return;
            }

            if (data.servers[interaction.guildId].gameStatus == status.lobby)
            {
                interaction.reply({ content: "Het spel is nog niet gestart, even geduld!!", ephemeral: true });
                return;
            }

            let embedRedPrivate = new EmbedBuilder()
                .setColor(0xFF0000)
                .setTitle('Tiem Rood');

            let embedRedPublic = new EmbedBuilder()
                .setColor(0xFF0000)
                .setTitle('Tiem Rood');

            let embedBluePrivate = new EmbedBuilder()
                .setColor(0x0000FF)
                .setTitle('Tiem Blauw');

            let embedBluePublic = new EmbedBuilder()
                .setColor(0x0000FF)
                .setTitle('Tiem Blauw');

            let isRed = false;
            let isBlue = false;

            redDescPublic = [];
            redDescPrivate = [];
            blueDescPublic = [];
            blueDescPrivate = [];

            for (let i = 0; i < data.servers[interaction.guildId].teams.red.length; i++)
            {
                let user = await GetUserById(interaction.guild, data.servers[interaction.guildId].teams.red[i].userId);
                let userName = user.displayName;

                if (data.servers[interaction.guildId].teams.red[i].userId == interaction.user.id)
                {
                    isRed = true;
                    redDescPrivate.push(data.servers[interaction.guildId].teams.red[i].piece.name + " " + userName + " (Dit ben jij!)");
                }
                else
                {
                    redDescPublic.push(userName);
                    redDescPrivate.push(data.servers[interaction.guildId].teams.red[i].piece.name + " " + userName);
                }
            }

            for (let i = 0; i < data.servers[interaction.guildId].teams.blue.length; i++)
            {
                let user = await GetUserById(interaction.guild, data.servers[interaction.guildId].teams.blue[i].userId);
                let userName = user.displayName;

                if (data.servers[interaction.guildId].teams.blue[i].userId == interaction.user.id)
                {
                    isBlue = true;
                    blueDescPrivate.push(data.servers[interaction.guildId].teams.blue[i].piece.name + " " + userName + " (Dit ben jij!)");
                }
                else
                {
                    blueDescPublic.push(userName);
                    blueDescPrivate.push(data.servers[interaction.guildId].teams.blue[i].piece.name + " " + userName);
                }
            }

            shuffleArray(redDescPrivate);
            shuffleArray(redDescPublic);
            shuffleArray(blueDescPrivate);
            shuffleArray(blueDescPublic);

            embedRedPublic.setDescription(redDescPublic.join("\n") || "leeg");
            embedRedPrivate.setDescription(redDescPrivate.join("\n") || "leeg");

            embedBluePublic.setDescription(blueDescPublic.join("\n") || "leeg");
            embedBluePrivate.setDescription(blueDescPrivate.join("\n") || "leeg");

            if (isRed)
            {
                return interaction.reply({ embeds: [embedRedPrivate, embedBluePublic], ephemeral: true });
            }
            else if (isBlue)
            {
                return interaction.reply({ embeds: [embedRedPublic, embedBluePrivate], ephemeral: true });
            }

            interaction.reply({ embeds: [embedRedPublic, embedBluePublic], ephemeral: true });
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: 'opdatum',
        description: 'Voer dit commando uit om de bot op te datumen!',
        admin: true,
        execute: function (interaction)
        {
            interaction.reply('OK Doei!').then(repl =>
            {
                setTimeout(() =>
                {
                    please.crash.the.bot.now();
                }, 1000);
            });
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: 'reset',
        description: 'Verwijder alle opgeslagen data en herstart!',
        admin: true,
        execute: function (interaction)
        {
            data = { servers: {} };
            interaction.reply('OK Doei!').then(repl =>
            {
                setTimeout(() =>
                {
                    please.crash.the.bot.now();
                }, 1000);
            });
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: 'maakadmin',
        description: 'Maak iemand adminder!',
        admin: true,
        options: [{ type: 6, name: "gebruiker", description: "Wiemst", required: true }],
        execute: function (interaction)
        {
            data.servers[interaction.guildId].admins.push(interaction.options.data[0].value);
            interaction.reply("<@" + interaction.options.data[0].value + "> is nu adminder van de Strategrobot in deze ober!");
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: 'roodrol',
        description: 'Selecteer de rol voor het rode tiem!',
        admin: true,
        options: [{ type: 8, name: "rol", description: "Welke rol?", required: true }],
        execute: function (interaction)
        {
            data.servers[interaction.guildId].redRole = interaction.options.data[0].value;
            interaction.reply("De rode rol is ingesteld!");
        }
    },
    {
        name: 'blauwrol',
        description: 'Selecteer de rol voor het blauwe tiem!',
        admin: true,
        options: [{ type: 8, name: "rol", description: "Welke rol?", required: true }],
        execute: function (interaction)
        {
            data.servers[interaction.guildId].blueRole = interaction.options.data[0].value;
            interaction.reply("De blauwe rol is ingesteld!");
        }
    },
    {
        name: 'doodrol',
        description: 'Selecteer de rol voor de dode spelers!',
        admin: true,
        options: [{ type: 8, name: "rol", description: "Welke rol?", required: true }],
        execute: function (interaction)
        {
            data.servers[interaction.guildId].deadRole = interaction.options.data[0].value;
            interaction.reply("De dode rol is ingesteld!");
        }
    },
    {
        name: 'verwijderroodrol',
        description: 'Verwijder de rol voor het rode tiem!',
        admin: true,
        options: [],
        execute: function (interaction)
        {
            data.servers[interaction.guildId].redRole = null;
            interaction.reply("De rode rol is weg!");
        }
    },
    {
        name: 'verwijderblauwrol',
        description: 'Verwijder de rol voor het blauwe tiem!',
        admin: true,
        options: [],
        execute: function (interaction)
        {
            data.servers[interaction.guildId].blueRole = null;
            interaction.reply("De blauwe rol is weg!");
        }
    },
    {
        name: 'verwijderdoodrol',
        description: 'Verwijder de rol voor de dode spelers!',
        admin: true,
        options: [],
        execute: function (interaction)
        {
            data.servers[interaction.guildId].deadRole = null;
            interaction.reply("De dode rol is weg!");
        }
    },
    {
        name: 'verwijderadmin',
        description: 'Maak iemand niet meer adminder!',
        admin: true,
        options: [{ type: 6, name: "gebruiker", description: "Wiemst", required: true }],
        execute: function (interaction)
        {
            if ("124136556578603009" == interaction.options.data[0].value)
            {
                interaction.reply({ content: "Mag niet!", ephemeral: true });
                return;
            }

            let index = data.servers[interaction.guildId].admins.indexOf(interaction.options.data[0].value);
            if (index >= 0)
            {
                data.servers[interaction.guildId].admins.splice(index);
                interaction.reply("<@" + interaction.options.data[0].value + "> is nu niet meer adminder van Strategrobot in deze ober!");
            }
            else
            {
                interaction.reply({ content: "<@" + interaction.options.data[0].value + "> is helemaal geen adminder!", ephemeral: true });
            }
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    },
    {
        name: 'statistieken',
        description: 'Leuke data!',
        options: [],
        execute: function (interaction)
        {
            let stats = GetAllUserStats(interaction.guildId, interaction.user.id);
            let embed = new EmbedBuilder()
                .setColor(0xFFAA00)
                .setTitle('Statistieken van ' + interaction.user.displayName);

            for (const [key, val] of Object.entries(stats)) 
            {
                embed.addFields({ name: key, value: val + "." });
            }

            interaction.reply({ embeds: [embed], ephemeral: true });
        },
        integration_types: [0, 1],
        contexts: [0, 1, 2]
    }
];

const rest = new REST({ version: '10' }).setToken(TOKEN);

(async () =>
{
    try
    {
        console.log('Started refreshing application (/) commands.');

        await rest.put(Routes.applicationCommands(CLIENT_ID), { body: commands });

        console.log('Successfully reloaded application (/) commands.');
    }
    catch (error)
    {
        console.error(error);
    }
})();

client.on('ready', () =>
{
    console.log(`Logged in as ${client.user.tag}!`);

    loadData(function (newData)
    {
        data = newData;
        delete data[null];
        console.log("DATA LOADED", data);
    });
});

function loadData (complete)
{
    fs.readFile('./botData.json', function read (err, data) 
    {
        if (err) 
        {
            data = "{}";
            console.log("no data found");
        }
        try
        {
            data = JSON.parse(data);
            console.log("savedata loaded:", data);
        }
        catch (e)
        {
            if (data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }

        if (data == null || data == {})
        {
            fs.readFile('./botDataBackup.json', function read (err, data)
            {
                if (err)
                {
                    data = "{}";

                    console.log("no data found in backup either");
                }
                try
                {
                    data = JSON.parse(data);
                    console.log("Backup restored");
                }
                catch (e)
                {
                    if (data == null)
                    {
                        data = {};
                        console.log("Memory backup broken, making new");
                        saveBackup = false;
                    }
                }
                initialized = true;
            });
        }
        else //successfully loaded
        {
            client.saveBackup(true);

            initialized = true;
        }

        data = data || {};

        data.servers = data.servers || {};

        complete(data);
    });
}

client.on('interactionCreate', async function (interaction)
{
    if (interaction.guildId === null)
    {
        interaction.reply("Je kunt mijn commando's niet in een persoonlijk bericht gebruiken.");
        return;
    }

    data.servers = data.servers || {};
    data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
    data.servers[interaction.guildId].admins = data.servers[interaction.guildId].admins || [];
    data.servers[interaction.guildId].teams = data.servers[interaction.guildId].teams || { red: [], blue: [] };
    data.servers[interaction.guildId].gameStatus = data.servers[interaction.guildId].gameStatus || status.none;

    if (interaction.isChatInputCommand())
    {
        for (let i = 0; i < commands.length; i++)
        {
            if (commands[i].name === interaction.commandName)
            {
                if (commands[i].admin)
                {
                    let admins = data.servers[interaction.guildId].admins || [];

                    console.log("adminder check:" + admins.indexOf(interaction.user.id), admins);

                    let isServerAdmin = false;
                    let isBotAdmin = admins.indexOf(interaction.user.id) >= 0;
                    let isHardcodedAdmin = interaction.user.id == admin;
                    let isOwner = false;

                    if (interaction.member.guild)
                    {
                        isServerAdmin = interaction.member.permissions.has(PermissionsBitField.Flags.Administrator);
                        isOwner = interaction.member.guild.ownerId == interaction.user.id;
                    }

                    let forbidden = (!isBotAdmin && !isHardcodedAdmin && !isServerAdmin && !isOwner);

                    console.log(forbidden);

                    if (forbidden)
                    {
                        console.log("blocked");
                        interaction.reply({ content: "Mag niet!", ephemeral: true });
                        return;
                    }
                }

                console.log("Current Data:", data);
                if (isAsyncFunction(commands[i].execute))
                    await commands[i].execute(interaction);
                else
                    commands[i].execute(interaction);

                saveData(data);
                return;
            }
        }
    }
    else if (interaction.isButton())
    {
        let user = interaction.user;
        let buttonId = interaction.customId;

        if (buttonId == "autojoin")
        {
            data.servers = data.servers || {};
            data.servers[interaction.guildId] = data.servers[interaction.guildId] || {};
            data.servers[interaction.guildId].autojoin = data.servers[interaction.guildId].autojoin || [];

            if (data.servers[interaction.guildId].autojoin.indexOf(interaction.user.id) < 0)
            {
                data.servers[interaction.guildId].autojoin.push(interaction.user.id);
                interaction.reply({ content: "Je wordt vanaf nu automatisch ingeschreven!", ephemeral: true });
            }
            else
            {
                interaction.reply({ content: "Je werd al automatisch ingeschreven!", ephemeral: true });
            }
        }
        else if (buttonId == "join")
        {
            for (let i = 0; i < data.servers[interaction.guildId].teams.red.length; i++)
            {
                if (data.servers[interaction.guildId].teams.red[i].userId == user.id)
                {
                    return interaction.reply({ content: "Je bent al ingeschreven, oelewapper.", ephemeral: true });
                }
            }

            for (let i = 0; i < data.servers[interaction.guildId].teams.blue.length; i++)
            {
                if (data.servers[interaction.guildId].teams.blue[i].userId == user.id)
                {
                    return interaction.reply({ content: "Je bent al ingeschreven, oelewapper.", ephemeral: true });
                }
            }

            if (data.servers[interaction.guildId].gameStatus != status.lobby)
            {
                interaction.reply({ content: "Helaas! Je kunt niet meer meedoen.", ephemeral: true });
                return;
            }

            let sortToBlue = data.servers[interaction.guildId].teams.red.length > data.servers[interaction.guildId].teams.blue.length;

            if (sortToBlue)
            {
                data.servers[interaction.guildId].teams.blue.push({ userId: user.id });
            }
            else
            {
                data.servers[interaction.guildId].teams.red.push({ userId: user.id });
            }

            let totalPlayers = data.servers[interaction.guildId].teams.red.length + data.servers[interaction.guildId].teams.blue.length;

            if (totalPlayers == 80)
            {
                interaction.deferReply();
                let failed = StartGame(interaction.guildId, interaction.guild);

                let embedRedPublic = new EmbedBuilder()
                    .setColor(0xFF0000)
                    .setTitle('Tiem Rood');
                let embedBluePublic = new EmbedBuilder()
                    .setColor(0x0000FF)
                    .setTitle('Tiem Blauw');

                redDescPublic = [];
                blueDescPublic = [];

                for (let i = 0; i < data.servers[interaction.guildId].teams.red.length; i++)
                {
                    let user = await GetUserById(interaction.guild, data.servers[interaction.guildId].teams.red[i].userId);
                    let userName = user.displayName;

                    redDescPublic.push(userName);
                }

                for (let i = 0; i < data.servers[interaction.guildId].teams.blue.length; i++)
                {
                    let user = await GetUserById(interaction.guild, data.servers[interaction.guildId].teams.blue[i].userId);
                    let userName = user.displayName;

                    blueDescPublic.push(userName);
                }

                shuffleArray(redDescPublic);
                shuffleArray(blueDescPublic);

                embedRedPublic.setDescription(redDescPublic.join("\n") || "leeg");
                embedBluePublic.setDescription(blueDescPublic.join("\n") || "leeg");

                interaction.editReply({ embeds: [embedRedPublic, embedBluePublic], content: "## De tiems zijn vol!\n# De strijd begint!\n\n" + (failed.length > 0 ? "Spelers die niet mee konden doen door een onbekende fout:\n" + failed.join("\n") : "") });
            }
            else
            {
                const joinButton = new ButtonBuilder()
                    .setCustomId("join")
                    .setLabel("Doe ook mee!")
                    .setStyle(ButtonStyle.Primary);

                const startButton = new ButtonBuilder()
                    .setCustomId("startnow")
                    .setLabel("Begin spel")
                    .setStyle(ButtonStyle.Secondary);

                const row = new ActionRowBuilder()
                    .addComponents(joinButton, startButton);

                interaction.reply({ content: "<@" + user.id + "> heeft zich ingeschreven. Er zijn nu " + totalPlayers + " inschrijvingen!", components: [row] });
            }
        }
        else if (buttonId == "startnow")
        {
            interaction.deferReply();
            let admins = data.servers[interaction.guildId].admins || [];

            console.log("adminder check:" + admins.indexOf(interaction.user.id), admins);
            let totalPlayers = data.servers[interaction.guildId].teams.red.length + data.servers[interaction.guildId].teams.blue.length;

            let isServerAdmin = false;
            let isBotAdmin = admins.indexOf(interaction.user.id) >= 0;
            let isHardcodedAdmin = interaction.user.id == admin;
            let isOwner = false;
            if (interaction.member.guild)
            {
                isServerAdmin = interaction.member.permissions.has(PermissionsBitField.Flags.Administrator);
                isOwner = interaction.member.guild.ownerId == interaction.user.id;
            }

            let forbidden = (!isBotAdmin && !isHardcodedAdmin && !isServerAdmin && !isOwner) || totalPlayers < 4;

            console.log(forbidden);

            if (forbidden)
            {
                console.log("blocked");
                interaction.editReply({ content: "Mag niet!", ephemeral: true });
                return;
            }

            let failed = StartGame(interaction.guildId, interaction.guild);

            let embedRedPublic = new EmbedBuilder()
                .setColor(0xFF0000)
                .setTitle('Tiem Rood');
            let embedBluePublic = new EmbedBuilder()
                .setColor(0x0000FF)
                .setTitle('Tiem Blauw');

            redDescPublic = [];
            blueDescPublic = [];

            for (let i = 0; i < data.servers[interaction.guildId].teams.red.length; i++)
            {
                let user = await GetUserById(interaction.guild, data.servers[interaction.guildId].teams.red[i].userId);
                let userName = user.displayName;

                redDescPublic.push(userName);
            }

            for (let i = 0; i < data.servers[interaction.guildId].teams.blue.length; i++)
            {
                let user = await GetUserById(interaction.guild, data.servers[interaction.guildId].teams.blue[i].userId);
                let userName = user.displayName;

                blueDescPublic.push(userName);
            }

            shuffleArray(redDescPublic);
            shuffleArray(blueDescPublic);

            embedRedPublic.setDescription(redDescPublic.join("\n") || "leeg");
            embedBluePublic.setDescription(blueDescPublic.join("\n") || "leeg");

            interaction.editReply({ embeds: [embedRedPublic, embedBluePublic], content: "# De strijd begint!\n\n" + (failed.length > 0 ? "Spelers die niet mee konden doen door een onbekende fout:\n" + failed.join("\n") : "") });
        }

        saveData(data);
    }

});

async function StartGame (serverId, guild)
{
    data.servers[serverId].gameStatus = status.playing;

    shuffleArray(data.servers[serverId].teams.red);
    shuffleArray(data.servers[serverId].teams.blue);

    let blueRole = await GetRoleForTeam(guild, data.servers[serverId].blueRole);
    let redRole = await GetRoleForTeam(guild, data.servers[serverId].redRole);

    let selector = 0;
    let redPiecesChosen = [];
    let bluePiecesChosen = [];

    for (let i = 0; i < piecesPerTeam.length; i++)
    {
        redPiecesChosen.push(0);
        bluePiecesChosen.push(0);
    }

    let couldNotJoin = [];

    for (let i = data.servers[serverId].teams.red.length - 1; i >= 0; i--)
    {
        try
        {
            let pickedRole = false;
            while (!pickedRole)
            {
                if (piecesPerTeam[selector].amount > redPiecesChosen[selector])
                {
                    redPiecesChosen[selector]++;
                    data.servers[serverId].teams.red[i].piece = piecesPerTeam[selector];
                    pickedRole = true;
                }
                else
                {
                    selector++;
                    if (selector >= piecesPerTeam.length)
                        selector = 0;
                }
            }
            selector++;
            if (selector >= piecesPerTeam.length)
                selector = 0;

            let user = await GetUserById(guild, data.servers[serverId].teams.red[i].userId);

            if (redRole)
                user.roles.add(redRole).catch(console.error);

            user.send("## [De strijd is begonnen](<https://discord.com/channels/" + serverId + ">)!\nWelkom in Tiem Rood!\n# Jij bent: De Rode " + data.servers[serverId].teams.red[i].piece.name + "!").catch(console.log);
            IncreaseUserStat(serverId, data.servers[serverId].teams.red[i].userId, "Gespeeld als " + data.servers[serverId].teams.red[i].piece.name, 1);
        }
        catch (e)
        {
            couldNotJoin.push("<@" + data.servers[serverId].teams.red[i].userId + ">");
            console.log("Speler kon niet meedoen:", data.servers[serverId].teams.red[i], e);
            data.servers[serverId].teams.red.splice(i, 1);
        }

        saveData(data);
    }

    selector = 0;

    for (let i = data.servers[serverId].teams.blue.length - 1; i >= 0; i--)
    {
        try
        {
            let pickedRole = false;
            while (!pickedRole)
            {
                if (piecesPerTeam[selector].amount > bluePiecesChosen[selector])
                {
                    bluePiecesChosen[selector]++;
                    data.servers[serverId].teams.blue[i].piece = piecesPerTeam[selector];
                    pickedRole = true;
                }
                else
                {
                    selector++;
                    if (selector >= piecesPerTeam.length)
                        selector = 0;
                }
            }
            selector++;
            if (selector >= piecesPerTeam.length)
                selector = 0;

            let user = await GetUserById(guild, data.servers[serverId].teams.blue[i].userId);

            if (blueRole)
                user.roles.add(blueRole).catch(console.error);

            user.send("## [De strijd is begonnen](<https://discord.com/channels/" + serverId + ">)!\nWelkom in Tiem Blauw!\n# Jij bent: De Blauwe " + data.servers[serverId].teams.blue[i].piece.name + "!").catch(console.log);
        }
        catch (e)
        {
            couldNotJoin.push("<@" + data.servers[serverId].teams.blue[i].userId + ">");
            console.log("Speler kon niet meedoen:", data.servers[serverId].teams.blue[i], e);
            data.servers[serverId].teams.blue.splice(i, 1);
        }
    }

    return couldNotJoin;
}

function IncreaseUserStat (serverId, userId, statName, amount)
{
    data.servers[serverId] = data.servers[serverId] || {};
    data.servers[serverId].stats = data.servers[serverId].stats || {};
    data.servers[serverId].stats[userId] = data.servers[serverId].stats[userId] || {};
    data.servers[serverId].stats[userId][statName] = data.servers[serverId].stats[userId][statName] || 0;

    data.servers[serverId].stats[userId][statName] -= -amount;
}

function GetUserStat (serverId, userId, statName)
{
    data.servers[serverId] = data.servers[serverId] || {};
    data.servers[serverId].stats = data.servers[serverId].stats || {};
    data.servers[serverId].stats[userId] = data.servers[serverId].stats[userId] || {};

    return data.servers[serverId].stats[userId][statName] || 0;
}

function GetAllUserStats (serverId, userId)
{
    data.servers[serverId] = data.servers[serverId] || {};
    data.servers[serverId].stats = data.servers[serverId].stats || {};
    return data.servers[serverId].stats[userId] || {};
}

async function StopGame (serverId, guild)
{
    data.servers[serverId].teams = { red: [], blue: [] };
    data.servers[serverId].gameStatus = status.none;

    saveData(data);
    await RemoveAllRoles(serverId, guild);
}

async function RemoveAllRoles (serverId, guild)
{
    let blueRole = await GetRoleForTeam(guild, data.servers[serverId].blueRole);
    let redRole = await GetRoleForTeam(guild, data.servers[serverId].redRole);
    let deadRole = await GetRoleForTeam(guild, data.servers[serverId].deadRole);

    if (blueRole)
    {
        blueRole.members.forEach((member, i) =>
        {
            setTimeout(() =>
            {
                member.roles.remove(blueRole);
            }, 1000 * i);
        });
    }

    if (redRole)
    {
        redRole.members.forEach((member, i) =>
        {
            setTimeout(() =>
            {
                member.roles.remove(redRole);
            }, 1000 * i);
        });
    }

    if (deadRole)
    {
        deadRole.members.forEach((member, i) =>
        {
            setTimeout(() =>
            {
                member.roles.remove(deadRole);
            }, 1000 * i);
        });
    }
}

async function KillPlayer (serverId, guild, playerId, killText)
{
    let blueRole = await GetRoleForTeam(guild, data.servers[serverId].blueRole);
    let redRole = await GetRoleForTeam(guild, data.servers[serverId].redRole);
    let deadRole = await GetRoleForTeam(guild, data.servers[serverId].deadRole);

    let user = await GetUserById(guild, playerId);

    if (blueRole)
        user.roles.remove(blueRole).catch(console.error);

    if (redRole)
        user.roles.remove(redRole).catch(console.error);

    if (deadRole)
        user.roles.add(deadRole).catch(console.error);

    user.send("## Je bent dood!\n" + killText).catch(console.log);
    IncreaseUserStat(serverId, playerId, "Hoe vaak gedood:", 1);

    for (let i = 0; i < data.servers[serverId].teams.red.length; i++)
    {
        if (data.servers[serverId].teams.red[i].userId == playerId)
        {
            data.servers[serverId].teams.red.splice(i, 1);
            return;
        }
    }

    for (let i = 0; i < data.servers[serverId].teams.blue.length; i++)
    {
        if (data.servers[serverId].teams.blue[i].userId == playerId)
        {
            data.servers[serverId].teams.blue.splice(i, 1);
            return;
        }
    }
}

client.saveBackup = function (silent = false)
{
    console.log("Writing Backup");
    fs.writeFile("./botDataBackup.json", JSON.stringify(data), function (err)
    {
        if (err)
        {
            console.log(err);
        }
    });
};

async function GetUserById (guild, userId)
{
    let user = await client.users.cache.get(userId);

    if (!user)
    {
        user = await client.users.fetch(userId);
    }

    return user;
}

async function GetRoleForTeam (guild, roleId)
{
    if (!roleId || !guild)
        return null;

    let role = await guild.roles.cache.get(roleId);

    if (!role)
    {
        role = await guild.roles.fetch(roleId);
    }

    return role;
}

saveData = function (newData)
{
    newData = newData || {};
    let json = JSON.stringify(newData);
    console.log("Saving...", newData, json);
    try
    {
        fs.writeFileSync("./botData.json", json);
    }
    catch (ex)
    {
        console.log("Error while saving!");
        console.log(ex);
    }
};

function shuffleArray (array)
{
    for (let i = array.length - 1; i > 0; i--)
    {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

client.login(TOKEN);